use std::fs::File;
use std::io::prelude::*;

pub fn take_n<T: Clone>(n: usize, src: &[T]) -> Vec<T> {
    src.iter().take(n).cloned().collect::<Vec<T>>()
}

pub fn read_file_strip_whitespace(filename: &str) -> String {
    let mut f = File::open(filename).expect("No file found.");
    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("Something went wrong with opening the file");
    contents.chars().filter(|c| *c != '\n').collect::<String>()
}

pub fn read_decode_b64(filename: &str) -> Vec<u8> {
    let string = read_file_strip_whitespace(filename);
    base64::decode(&string).unwrap()
}

pub fn cookie_value_parser(text: &str) -> String {
    let mut res = String::from("{");
    for part in text.split('&') {
        let mut pair = String::from(part);
        let v = pair.split_off(part.find('=').expect("No = character found") + 1);
        pair.pop();
        res = res + "\n  " + &pair + ": \'" + &v + "\',";
    }
    res.pop();
    res = res + "\n}";
    res
}

pub fn profile_encoder(email: &str) -> Option<String> {
    if email.contains('&') || email.contains('=') {
        return None;
    }
    let mut user = String::from("email=");
    user = user + email + "&uid=10&role=user";
    Some(user)
}

pub fn encode_encrypt(email: &str, key: &Vec<u8>) -> Option<Vec<u8>> {
    use block::encrypt_aes_128_ecb;

    let profile = match profile_encoder(email) {
        Some(x) => x,
        None => { return None; }
    };

    let ct = encrypt_aes_128_ecb(profile.as_bytes(), key, None);
    Some(ct) 
}

pub fn decrypt_decode(ct: &[u8], key: &Vec<u8>) -> Option<String> {
    use block::decrypt_aes_128_ecb;
    use conversions::bytes_to_string;

    let pt = decrypt_aes_128_ecb(ct, key, None);
    let pt = bytes_to_string(&pt);
    let profile = cookie_value_parser(&pt);
    Some(profile)
}
