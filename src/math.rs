use std::collections::{BTreeMap, VecDeque};
use conversions;

fn eng_freqs() -> BTreeMap<char, f64> {
    //frequencies corresponding ASCII values or a-z and A-Z
    let freqs = [8.167, 1.492, 2.782, 4.253, 12.702, 2.228, 2.015, 6.094, 6.966, 0.153, 0.772, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987, 6.327, 9.056, 2.758, 0.978, 2.360, 0.150, 1.974, 0.074];
    let lower_freqs = (0..26).into_iter().map(|x| (x + 'a' as u8) as char).zip(freqs.iter());
    let upper_freqs = (0..26).into_iter().map(|x| (x + 'A' as u8) as char).zip(freqs.iter());
    let mut map = BTreeMap::new();
    for (ch, fr) in lower_freqs.chain(upper_freqs) {
        map.insert(ch, *fr);
    }
    map
}

pub fn ascii_fitness(candidate: &[u8]) -> f64 {
    let mut total_fit = 0.0;
    let freq_table = eng_freqs();
    for c in candidate.iter() {
        if *c == 32 as u8 {
            total_fit += 20.0;
        }
        match freq_table.get(&(*c as char)) {
            Some(i) => total_fit += i,
            _ => continue,
        }
    }
    total_fit
}

/// Calculates the Hamming distance of byte-vectors of same length
/// 
/// # Examples
/// 
/// ```
/// use lib::conversions::str_to_bytes;
/// use lib::math::hamming_distance;
/// 
/// let fst = str_to_bytes(&"this is a test");
/// let snd = str_to_bytes(&"wokka wokka!!!");
/// let diff = hamming_distance(&fst, &snd).unwrap();
/// assert_eq!(diff, 37);
/// ```
pub fn hamming_distance(fst: &[u8], snd: &[u8]) -> Option<u64> {
    if fst.len() != snd.len() {
        return None;
    }
    let diff = fst.iter().zip(snd.iter())
                .map(|(a,b)| *a ^ *b)
                .fold(0, |acc, byte| acc + byte.count_ones() as u64);
    Some(diff)
}

pub fn calc_avg_hamming(ksize: usize, ct: &[u8]) -> f32 {
    use util::take_n;
    static LIMIT: usize = 4;

    let mut blocks = VecDeque::new();
    for i in 0..LIMIT {
        blocks.push_back(take_n(ksize, &ct[i*ksize..]));
    }

    let mut total_distance = 0.0;
    loop {
        let cur_block = blocks.pop_front().unwrap();

        if blocks.is_empty() {
            break;
        }
        for other in blocks.iter() {
            let dist = match hamming_distance(&cur_block, other) {
                Some(d) => d,
                None => panic!("Error: ciphertext is too short to perform the statistical analysis.")
            };
            total_distance += dist as f32 / ksize as f32;
        }
    }
    total_distance as f32 / LIMIT as f32
}

pub fn count_repeating_blocks(bytes: &[u8], chunk_size: usize) -> u32 {
    let mut map = BTreeMap::new();
    let mut reps = 0;
    for block in bytes.chunks(chunk_size) {
        let bstr = conversions::bytes_to_hex(&block.iter().cloned().collect::<Vec<u8>>());
        let entry = map.entry(bstr).or_insert(0);
        if *entry > 0 {
            reps += 1;
        }
        *entry += 1;
    }
    reps
}