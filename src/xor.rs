extern crate openssl;

use xor::openssl::bn::{BigNum};
use xor::openssl::string::{OpensslString};
use math;

static KEYSIZE_MAX: usize = 40;

pub fn xor_hex(fst: &str, snd: &str) -> Option<OpensslString> {
    if fst.len() != snd.len() {
        return None;
    }
    let fst = BigNum::from_hex_str(fst).unwrap();
    let snd = BigNum::from_hex_str(snd).unwrap();
    let xor_bytes = fst.to_vec().iter().zip(snd.to_vec().iter())
                        .map(|(a,b)| a^b)
                        .collect::<Vec<u8>>();
    let res = BigNum::from_slice(&xor_bytes[..]).unwrap();
    return Some(res.to_hex_str().unwrap());
}

pub fn xor_bytes(fst: &[u8], snd: &[u8]) -> Option<Vec<u8>> {
    if fst.len() != snd.len() {
        return None;
    }
    let xor_bytes = fst.iter().zip(snd.iter())
        .map(|(a,b)| a^b)
        .collect::<Vec<u8>>();
    Some(xor_bytes)
}

pub fn xor_bytes_vs_one(bytes: &[u8], byte: u8) -> Vec<u8> {
    let res: Vec<u8> = bytes.iter().map(|&b| &b ^ byte).collect();
    return res;
}

pub fn brute_xor_alphabetically(candidate: &[u8]) -> u8 {

    let mut likely_cypher: u8 = 0;
    let mut best_fit = 0.0;

    let lower_alpha = (0..26).into_iter().map(|x| x + 'a' as u8);
    let upper_alpha = (0..26).into_iter().map(|x| x + 'A' as u8);
    let alphas = lower_alpha.chain(upper_alpha);

    for c in alphas {
        let current = xor_bytes_vs_one(candidate, c as u8);
        let fit = math::ascii_fitness(&current[..]);
        if fit > best_fit {
            likely_cypher = c;
            best_fit = fit;
        
        }
    }
    likely_cypher
}

pub fn brute_xor_single(candidate: &[u8]) -> u8 {
    let mut likely_cypher = 0 as u8;
    let mut best_fit = 0.0;

    for byte in 0..u8::max_value() {
        let current = xor_bytes_vs_one(candidate, byte);
        let fit = math::ascii_fitness(&current[..]);
        if fit > best_fit {
            likely_cypher = byte;
            best_fit = fit;
        }
    }
    likely_cypher
}

pub fn xor_repeating_key(src: &[u8], key: &[u8]) -> Vec<u8> {
    let mut res = Vec::new();
    let mut ctr: usize = 0;
    let limit = key.len();

    for c in src.iter() {
        res.push((c ^ key[ctr]) as u8);
        ctr = (ctr + 1) % limit;
    }
    res
}

pub fn break_repeating_key_xor(ciphertext: &[u8]) -> Vec<u8> {
    let mut best_len = 2e16;
    let mut k = 0;
    for ksize in 2..KEYSIZE_MAX {
        let cand_len = math::calc_avg_hamming(ksize, ciphertext);
        if cand_len < best_len {
            best_len = cand_len;
            k = ksize;
        }
    }
    let ksize = k;
    println!("Most likely key length is {} with fitness {}.", ksize, best_len);

    let mut key = Vec::new();
    for i in 0..ksize {
        let mut candidate = Vec::new();
        for chunk in ciphertext.chunks(ksize) {
            if i >= chunk.len() {
                continue;
            }
            candidate.push(chunk[i]);
        }
        let key_char = brute_xor_single(&candidate[..]);
        key.push(key_char);
    }
    key
}