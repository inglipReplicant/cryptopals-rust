extern crate hex;
extern crate base64;
extern crate lib;

fn main() {
    println!("Running 02-04: Byte-at-a-time ECB decryption (Simple)\n");
    let ct = lib::util::read_decode_b64("input_files/02/02-04.txt");
    let pt = lib::cryptan::aes_ecb_attack(&ct[..]).unwrap();
    let pt = lib::conversions::bytes_to_string(&pt);
    println!("\nPlaintext: {}", pt);
}