extern crate hex;
extern crate lib;

use lib::cryptan::ecb_cut_and_paste;
use lib::block::gen_rand_key;

fn main() {
    println!("Running 02-0: ECB: Cut-and-paste\n");
    let key = gen_rand_key();
    let profile = ecb_cut_and_paste(&key);
    println!("Crafted user: {}", profile);
}
