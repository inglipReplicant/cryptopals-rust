extern crate hex;
extern crate lib;
extern crate openssl;

fn main() {
    println!("Running 02-02: AES with CBC mode\n");
    let bytes = lib::util::read_decode_b64("input_files/02/02-02.txt");
    let key = "YELLOW SUBMARINE".as_bytes();
    let iv = [0 as u8; 16];
    
    let decrypted = lib::block::decrypt_aes_128_cbc(&bytes[..], &key, &iv);
    println!("{}", lib::conversions::bytes_to_hex(&decrypted));
}