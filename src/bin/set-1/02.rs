extern crate lib;

use lib::xor::xor_hex;

fn main() {
    println!("Running 01-02: Fixed XOR\n");
    let fst = "1c0111001f010100061a024b53535009181c";
    let snd = "686974207468652062756c6c277320657965";
    let xor = xor_hex(&fst, &snd).unwrap();
    println!("{}", xor);
}
