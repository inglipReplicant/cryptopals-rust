extern crate hex;
extern crate lib;

use lib::xor::xor_repeating_key;
use lib::conversions::{bytes_to_hex, str_to_bytes};

fn main() {
    println!("Running 01-05: Implement repeating-key XOR\n");
    let src = str_to_bytes(&"Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal");
    let key = str_to_bytes(&"ICE");

    let cyphertext = xor_repeating_key(&src, &key);
    let cyphertext = bytes_to_hex(&cyphertext);
    println!("{}\n", cyphertext);

    //just some public key...
    let src = str_to_bytes(&"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCu7XO7vqYiBqka21JV0K/sGxfaE1VfG6pqSo7J3Y2KmGXp969SWwJjfJtjDD+r9rolcfkwk/ZcdpLD4fWxqgY3vQdEutqqzAKMS471n0WybU5EeEqp1eLN+UXYbF28C6VAr1/uufttv6CITlwhxdzTzMPGl1wy1bbSmBO1cqgBdPkrdLvW65nneOZWRXetTAc45VagmW07Y1W29/Opb7fOf+oMSOOj4ihEp/RwWrCX+Mq9aPR6fsM1WGjVlw1BBboIG5EvexJb10p9kiDTglgrElUxuhsMY91CmAzBM9ApvQmRUc4+jNyvbDCLHBsj9yWAHOmNPXOGfFjGaEkJ9F/z enoch@Epiphyte");
    let cyphertext = xor_repeating_key(&src, &key);
    let cyphertext = bytes_to_hex(&cyphertext);
    println!("{}", cyphertext);
}