extern crate hex;
extern crate lib;
extern crate openssl;

use lib::*;
use openssl::symm::{decrypt, Cipher};

fn main() {
    println!("Running 01-07: AES in ECB mode\n");

    let ciphertext = util::read_decode_b64("input_files/01/01-07.txt");
    let key = "YELLOW SUBMARINE".as_bytes();
    let cipher = Cipher::aes_128_ecb();

    let plaintext = decrypt(cipher, key, None, &ciphertext[..]).expect("eek");
    println!("{}", conversions::bytes_to_string(&plaintext));
}