extern crate hex;
extern crate lib;

use lib::math::*;
use lib::xor::*;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    println!("Running 01-04: Detect single-character XOR\n");
    let filename = "input_files/01/01-04.txt";
    let mut f = File::open(filename).expect("ERROR: File not found.");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");

    let mut best = Vec::new();
    let mut best_fit = 0.0;
    let mut cracked_cypher = 0;
    for line in contents.lines() {
        let bytes = hex::decode(line).unwrap();
        let cypher = brute_xor_single(&bytes);
        let fit = ascii_fitness(&bytes);

        if fit > best_fit {
            best_fit = fit;
            best = Vec::from(line);
            cracked_cypher = cypher;
        }
    }
    let best: String = best.iter().map(|x| *x as char).collect();
    println!("Cyphertext that is most likely to be encrypted is {:?}", best);
    println!("Its fitness is {}", best_fit);
    let best_bytes = hex::decode(&best[..]).unwrap();
    let plaintext = xor_bytes_vs_one(&best_bytes, cracked_cypher);
    let plaintext: String = plaintext.iter().map(|x| *x as char).collect();
    println!("Decrypted with cypher {}: {:?}", cracked_cypher, plaintext);
}
