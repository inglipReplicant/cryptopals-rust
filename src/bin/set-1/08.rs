extern crate hex;
extern crate lib;
extern crate openssl;

use lib::*;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    println!("Running 01-08: Detect AES in ECB mode\n");
    let mut file = File::open("input_files/01/01-08.txt").expect("No file found");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Error while reading the file");

    let likeliest_bytes = cryptan::aes_ecb_detect(&contents);

    println!("Most likely encrypted line is {}", conversions::bytes_to_hex(&likeliest_bytes));
}