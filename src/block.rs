extern crate openssl;
extern crate rand;

use block::openssl::symm::{encrypt, decrypt, Cipher, Crypter, Mode};
use block::rand::prelude::*;
//use conversions::{bytes_to_hex, byte_slice_to_hex};

pub static AES_BLOCK_LEN: usize = 16;

pub fn gen_rand_key() -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let mut key: Vec<u8> = Vec::new();
    for _ in 0..AES_BLOCK_LEN {
        key.push(rng.gen_range(0, 256) as u8);
    }
    key
}

pub fn encrypt_aes_128_ecb(pt: &[u8], key: &[u8], iv: Option<&[u8]>) -> Vec<u8> {
    let cipher = Cipher::aes_128_ecb();
    let ciphertext = encrypt(cipher, key, iv, pt)
        .expect("Unable to encrypt AES message in ECB mode.");
    ciphertext
}

pub fn encrypt_aes_128_ecb_no_pad(pt: &[u8], key: &[u8], iv: Option<&[u8]>) -> Vec<u8> {
    let mut ct = Vec::new();
    for chunk in pt.chunks(AES_BLOCK_LEN) {
        if chunk.len() == AES_BLOCK_LEN {
            ct.append(&mut encrypt_aes_128_ecb_block(&chunk[..], key, iv));
        } else {
            let padded = pad_block_pkcs7(&chunk[..], AES_BLOCK_LEN).unwrap();
            ct.append(&mut encrypt_aes_128_ecb_block(&padded[..], key, iv));
        }
    }
    ct
}

pub fn encrypt_aes_128_ecb_block(pt: &[u8], key: &[u8], iv: Option<&[u8]>) -> Vec<u8> {
    let cipher = Cipher::aes_128_ecb();
    let mode = Mode::Encrypt;
    let mut crypter = Crypter::new(cipher, mode, key, iv).expect("Unable to construct crypter.");
    crypter.pad(false);

    let mut ct = vec![0; AES_BLOCK_LEN + cipher.block_size()];
    let mut count = crypter.update(pt, &mut ct).unwrap();
    count += crypter.finalize(&mut ct).unwrap();
    ct.truncate(count);
    ct
}

pub fn decrypt_aes_128_ecb(ct: &[u8], key: &[u8], iv: Option<&[u8]>) -> Vec<u8> {
    let cipher = Cipher::aes_128_ecb();
    let plaintext = decrypt(cipher, key, iv, ct)
        .expect("Unable to decrypt AES message in ECB mode.");
    plaintext
}

pub fn decrypt_aes_128_ecb_block(ct: &[u8], key: &[u8], iv: Option<&[u8]>) -> Vec<u8> {
    let cipher = Cipher::aes_128_ecb();
    let mode = Mode::Decrypt;
    let mut decrypter = Crypter::new(cipher, mode, key, iv).expect("Unable to construct crypter.");
    decrypter.pad(false);

    let mut pt = vec![0; AES_BLOCK_LEN + cipher.block_size()];
    let mut count = decrypter.update(ct, &mut pt).unwrap();
    count += decrypter.finalize(&mut pt).unwrap();
    pt.truncate(count);
    pt
}

pub fn pad_block_pkcs7(bytes: &[u8], block_len: usize) -> Option<Vec<u8>> {
    if bytes.len() > block_len {
        return None
    }
    let diff = block_len - bytes.len();
    let mut res = Vec::from(bytes);
    for _ in 0..diff {
        res.push(diff as u8);
    }
    Some(res)
}

pub fn validate_pkcs7(bytes: &[u8]) -> Option<Vec<u8>> {
    let last = bytes.last().unwrap();
    if *last > 0xF {
        return None;
    }

    let incorrect = bytes
        .iter()
        .rev()
        .take(*last as usize)
        .any(|x| x != last);
    
    if incorrect {
        return None;
    }
    let mut new = bytes
        .iter()
        .cloned()
        .collect::<Vec<u8>>();
    
    new.truncate(bytes.len() - *last as usize);
    Some(new)
}

pub fn encrypt_aes_128_cbc(pt: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
    use xor::xor_bytes;
    let xorred_pt;
    let mut pt = pt;

    if pt.len() == 0 {
        return Vec::new();
    } else if pt.len() < AES_BLOCK_LEN {
        let padded = pad_block_pkcs7(pt, AES_BLOCK_LEN).expect("Unable to pad bytes.");
        xorred_pt = xor_bytes(&padded[..], iv).expect("Bad padded length while encrypting.");
        pt = &xorred_pt[..]
    } else {
        xorred_pt = xor_bytes(&pt[0..AES_BLOCK_LEN], iv).expect("Incorrect xor length.");
    }

    let mut ct = encrypt_aes_128_ecb_block(&xorred_pt[..], key, None);
    let next_iv = ct.iter().cloned().collect::<Vec<u8>>();

    if pt.len() < AES_BLOCK_LEN {
        return ct;
    } else {
        ct.append(&mut encrypt_aes_128_cbc(&pt[AES_BLOCK_LEN..], key, &next_iv[..AES_BLOCK_LEN]));
    }
    ct
}

pub fn decrypt_aes_128_cbc(ct: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
    let mut pt = construct_decrypted_aes_128_cbc(ct, key, iv);
    let aes_len = AES_BLOCK_LEN as u8;
    let last_len = *pt.get(pt.len() - 1).unwrap();
    if last_len < aes_len {

        let ctr = pt[(pt.len() - (last_len as usize) - 1)..]
            .iter()
            .fold(0, |acc, x| {
                let mut val = 0;
                if *x == last_len {
                    val = 1;
                }
                acc + val
            });
        if ctr == last_len && (ctr as usize) < pt.len() {
            for _ in 0..ctr {
                let _ = pt.pop();
            }
        }
    }
    pt
}

fn construct_decrypted_aes_128_cbc(ct: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
    use xor::xor_bytes;
    if ct.len() == 0 {
        return Vec::new();
    }

    let xord_pt = decrypt_aes_128_ecb_block(&ct[..AES_BLOCK_LEN], key, None);
    let mut pt = xor_bytes(&xord_pt[..], &iv[..AES_BLOCK_LEN]).expect("Incorrect xor length.");
    pt.append(&mut construct_decrypted_aes_128_cbc(&ct[AES_BLOCK_LEN..], key, &ct[..AES_BLOCK_LEN]));
    pt
}